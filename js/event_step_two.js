
      jQuery(function(){
        jQuery("#geocomplete").geocomplete({
          map: ".map_canvas",
          details: "form",
          blur: true,
          geocodeAfterResult: true
        });

        jQuery("#find").click(function(){
          jQuery("#geocomplete").trigger("geocode");
        });
      });

      jQuery(document).ready(function(){
        jQuery("#geocomplete").keyup(function(){
          var value = jQuery("#geocomplete").val().length;
          if (!value) {
            jQuery('#event-venue-address-details').css('display', 'none');
            jQuery('#manually-enter-address').prop('checked', false);
          }
        });

        jQuery("#online-event-check").click(function(event) {
          if (jQuery(this).is(":checked")){
            jQuery(".map_canvas").hide();
            jQuery("#event-venue-address-details").hide();
            jQuery("#geocomplete").val('Some Value');
            jQuery("#manually-enter-address").prop("checked", false);
            jQuery("#manually-enter-address").prop("disabled", false);
            jQuery('.form-item-event-venue').css('display', 'block');
            jQuery("#geocomplete").prop("disabled", true);
          }
          else{
            jQuery(".map_canvas").show();
            //jQuery("#event-venue-address-details").show();
            jQuery("#geocomplete").val('');
             jQuery("#geocomplete").prop("disabled", false);
          }
        });
        jQuery(".form-item-add-location input").click(function(event) {
          if (jQuery(this).is(":checked")){
            jQuery(".form-item-add-location").hide();
            jQuery(".map_canvas").show();
            jQuery("#geocomplete").show();
            jQuery("#event-venue-address-details").hide();
            jQuery(".form-item-enter-addr").show();
            jQuery("#geocomplete").val('');
            jQuery("#geocomplete").prop("disabled", false);
            jQuery("#online-event-check").prop("checked", false);
            jQuery(".form-item-add-location input").prop("checked", false);
          }
        });
        jQuery("#event-venue-longitude").keyup(function(){
          var lat_value = jQuery("#event-venue-latitude").val().length;
          var lag_value = jQuery("#event-venue-longitude").val().length;
          // if (lat_value != '' && lag_value != '') {
          //   var latlng = {lat: parseFloat(lat_value), lng: parseFloat(lag_value)};
          //   map: ".map_canvas",
          //   details: "form",
          //   blur: true,
          //   geocodeAfterResult: true,
          //   loction: latlng
          // }
        });

        if (Drupal.ajax) {
          jQuery('.ajax-example-hide').hide();
        }

      }); 
