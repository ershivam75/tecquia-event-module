<?php

/**
 * @file
 * Contains \Drupal\event_form\Form\EventStepTwoForm.
 */

namespace Drupal\event_form\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class EventStepTwoForm extends MultistepEventForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'event_step_two_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //$form = parent::buildForm($form, $form_state);

    // $form['#attached']['js'][] = array(
    //   'type' => 'file',
    //   'data' => drupal_get_path('module','event_form') . '/js/event_step_two.js',
    // );

    // $form['#attached']['js'] = array(
    //  drupal_get_path('module','event_form') . '/js/event_step_two.js',
    // );
    //drupal_add_js(drupal_get_path('module', 'event_form') . "/js/event_step_two.js");

    $build['#attached']['library'][] = 'event_form/event_form';

    $form['#prefix'] = '<div id="event-step-two">';
    $form['#suffix'] = '</div>';

    

    $form['event_venue'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Where are you Hosting Your Event?"),
      '#required' => TRUE,
      '#attributes' => array(
        'placeholder' => t('Type in an address'),
        'id' => 'geocomplete',
        'class' => 'text-geo',
      ),
      // '#ajax' => [
      //   'wrapper' => 'event-venue-address-details',
      //   'callback' => '::eventAddressDetailsCallback',
      // ],
      '#prefix' => '<div class="single-field">',
      '#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          ':input[name="enter_addr"]' => array('checked' => False),
        ),
      ),
    );

     $form['enter_addr'] = array(
      '#type' => 'checkbox',
      '#title' => '<p><span>Enter</span> Address</p>',
      '#attributes' => array(
        'id' => 'manually-enter-address',
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="online_event"]' => array('checked' => False),
        ),
      ),
    );

    $form['add_location'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add a Location'),
      '#attributes' => array(
        'id' => 'add-location-event',
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="online_event"]' => array('checked' => TRUE),
        ),
      ),
      // '#ajax' => [
      //   'wrapper' => 'event-step-two',
      //   'callback' => '::eventOnlineAddressCallback',
      // ],
    );

    $form['online_event'] = array(
      '#type' => 'checkbox',
      '#title' => t('Online Event'),
      '#attributes' => array(
        'id' => 'online-event-check',
      ),
      // '#ajax' => [
      //   //'wrapper' => 'event-venue-address-details',
      //   'callback' => '::eventOnlineAddressCallback',
      // ],
    );

   

    $form['details'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'event-venue-address-details'),
      //'#prefix' => '<div class="map_canvas">',
      //'#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          ':input[name="enter_addr"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['details']['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Enter the Venue's Name"),
      '#attributes' => array('id' => 'event-venue-name'),
    );
    $form['details']['street_number'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Address 1"),
      '#attributes' => array('id' => 'event-venue-street-number'),
    );
    $form['details']['route'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Address 2"),
      '#attributes' => array('id' => 'event-venue-route'),
    );
    $form['details']['sublocality'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Address 3"),
      '#attributes' => array('id' => 'event-venue-sublocality'),
    );
    $form['details']['locality'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("City"),
      '#attributes' => array('id' => 'event-venue-locality'),
    );
    $form['details']['administrative_area_level_1'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("County / State"),
      '#attributes' => array('id' => 'event-venue-county-state'),
    );
    $form['details']['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Post Code / Zip"),
      '#attributes' => array('id' => 'event-venue-postal-code'),
    );
    $form['details']['country'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Country"),
      '#attributes' => array('id' => 'event-venue-country'),
    );
    $form['details']['formatted_phone_number'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Venue Phone"),
      '#attributes' => array('id' => 'event-venue-phone-number'),
    );
    $form['details']['lat'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Latitude"),
      '#attributes' => array('id' => 'event-venue-latitude'),
    );
    $form['details']['lng'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("Longitude"),
      '#attributes' => array('id' => 'event-venue-longitude'),
    );
    $form['details']['show_map_event_page'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show map on Event Page'),
      '#attributes' => array('checked' => 'checked'),
    );

    $form['map_canvas'] = array(
      '#type' => 'item',
      //'#attributes' => array('class' => 'map_canvas'),
      '#prefix' => '<div class="map_canvas">',
      '#suffix' => '</div>',
    );

    $form['actions']['previous'] = array(
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => array(
        //'class' => array('button'),
      ),
      //'#weight' => 0,
      '#url' => Url::fromRoute('event_form.step_one'),
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      //'#button_type' => 'primary',
      //'#weight' => 10,
    );


    return $form;
  }
  public function eventOnlineAddressCallback(array &$form, FormStateInterface $form_state)
  {
    //$form['event_venue']['#value'] = 'some value';
    //return $form['event_venue'];
    $form_state->setRebuild();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$this->store->set('age', $form_state->getValue('age'));
    //$this->store->set('location', $form_state->getValue('location'));

    // Save the data
    //parent::saveData();
    $form_state->setRedirect('event_form.step_three');
    //$form_state->setRebuild(TRUE);
  }
}
