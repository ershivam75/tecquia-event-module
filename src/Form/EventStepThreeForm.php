<?php

/**
 * @file
 * Contains \Drupal\event_form\Form\EventStepThreeForm.
 */

namespace Drupal\event_form\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class EventStepThreeForm extends MultistepEventForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'event_step_three_form';
  }

    public function buildForm(array $form, FormStateInterface $form_state) {
    // Disable caching for the form
      $form['#cache'] = ['max-age' => 0];
      
      // Do not flatten nested form fields
      $form['#tree'] = TRUE;
      
      $form['field_container'] = array(
        '#type' => 'container',
        '#weight' => 80,
        '#tree' => TRUE,
        // Set up the wrapper so that AJAX will be able to replace the fieldset.
        '#prefix' => '<div id="js-ajax-elements-wrapper">',
        '#suffix' => '</div>',
      );
      
      if ($form_state->get('field_deltas') == '') {
        $form_state->set('field_deltas', range(0, 0));
      }
      
      $field_count = $form_state->get('field_deltas');
    
      foreach ($field_count as $delta) {
          $form['field_container'][$delta] = array(
            '#type' => 'container',
            '#attributes' => array(
              'class' => array('container-inlines'),
            ),
            '#tree' => TRUE,
          );
      

          $form['field_container'][$delta]['ticket_type'] = array (
            '#type' => 'radios',
            '#title' => ('Select the type of ticket you need:'),
            '#options' => array(
              'Free Ticket' =>t('Free Ticket'),
              'Paid Ticket' =>t('Paid Ticket')
            ),
            '#size' => 10,
            '#name' => 'ticket_type' . $delta,
        );
        
        
          
            $form['field_container'][$delta]['ticket_name'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Enter the ticket name'),
                '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
               // '#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("Early Bird, RSVP, Premium"),
              ),
            ];
            $form['field_container'][$delta]['quantity_available'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Quantity Available'),
                '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
              //  '#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("Enter the Quantity"),
              ),
            ];
            $form['field_container'][$delta]['price'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Price'),
                '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
                //'#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("Free"),
              ),
            ];
            $form['field_container'][$delta]['public'] = array(
              '#type' => 'checkbox',
              '#title' => t('Public'),
              '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
            );
            $form['field_container'][$delta]['private'] = array(
              '#type' => 'checkbox',
              '#title' => t('Private'),
              '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
            );
            $form['field_container'][$delta]['candidate_confirmation'] = array (
              '#type' => 'radios',
              '#title' => ('Apply Fee'),
              '#options' => array(
                'Yes' =>t('Yes'),
                'No' =>t('No')
              ),
              '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
            );

            $form['field_container'][$delta]['ticket_settings'] = array(
              '#type' => 'details',
              '#title' => t('Ticket Settings'),
              '#states' => array(
              'visible' => array(
                'input[name="field_container['.$delta.'][ticket_type]"]' => array('value' => 'Paid Ticket'),
              ),
          ),
            );
            $form['field_container'][$delta]['ticket_settings']['ticket_description']= [
                '#type' => 'textfield',
                '#title' => $this->t('Ticket Description'),
               // '#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("Tell your attendees more about this ticket type"),
              ),
            ];
            $form['field_container'][$delta]['ticket_settings']['show_description_on_event'] = array (
              '#type' => 'radios',
              '#title' => ('Show description on event page'),
              '#options' => array(
                'Yes' =>t('Yes'),
                'No' =>t('No')
              ),
            );
            $form['field_container'][$delta]['ticket_settings']['sales_channel'] = array (
              '#type' => 'select',
              '#title' => ('Sales Channel'),
              '#options' => array(
                'Everywhere' => t('Everywhere'),
                'test1' => t('test1'),
                'test2' => t('test2'),
              ),
            );
            $form['field_container'][$delta]['ticket_settings']['ticket_sales_end'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Ticket Sales End'),
               // '#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("01:00"),
              ),
            ];
            $form['field_container'][$delta]['ticket_settings']['hours'] = array (
              '#type' => 'select',
              '#title' => ('Hour (s)'),
              '#options' => array(
                'Everywhere' => t('Everywhere'),
                'test1' => t('test1'),
                'test2' => t('test2'),
              ),
            );
            $form['field_container'][$delta]['ticket_settings']['starts'] = array (
              '#type' => 'select',
              '#title' => ('Start (s)'),
              '#options' => array(
                'Everywhere' => t('Everywhere'),
                'test1' => t('test1'),
                'test2' => t('test2'),
              ),
            );
            $form['field_container'][$delta]['ticket_settings']['special_requirements'] = [
                '#type' => 'textarea',
                '#title' => $this->t('Special Requirements'),
               // '#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("Tell your attendees more about this ticket type"),
              ),
            ];
            $form['field_container'][$delta]['ticket_settings']['hide_ticket'] = array (
              '#type' => 'radios',
              '#title' => ('Hide this ticket type'),
              '#options' => array(
                'Yes' =>t('Yes'),
                'No' =>t('No')
              ),
            );
            $form['field_container'][$delta]['ticket_settings']['minimum'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Minimum'),
                //'#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("01"),
              ),
            ];
            $form['field_container'][$delta]['ticket_settings']['maximaum'] = [
                '#type' => 'textfield',
                '#title' => $this->t('Maximaum'),
               // '#required' => TRUE,
                '#attributes' => array(
                'placeholder' => t("10"),
              ),
            ];
            
        
      
        $form['field_container'][$delta]['remove_name'] = array(
          '#type' => 'submit',
          '#value' => t('X'),
          '#submit' => array('::mymoduleAjaxExampleAddMoreRemove'),
          '#ajax' => array(
            'callback' => '::mymoduleAjaxExampleAddMoreRemoveCallback',
            'wrapper' => 'js-ajax-elements-wrapper',
          ),
          '#weight' => -50,
          '#attributes' => array(
            'class' => array('button-small'),
          ),
          '#name' => 'remove_name_' . $delta,
        );
      }
    
      $form['field_container']['add_name'] = array(
        '#type' => 'submit',
        '#value' => t('Add one more'),
        '#submit' => array('::mymoduleAjaxExampleAddMoreAddOne'),
        '#ajax' => array(
          'callback' => '::mymoduleAjaxExampleAddMoreAddOneCallback',
          'wrapper' => 'js-ajax-elements-wrapper',
        ),
      );

      $form['actions']['previous'] = array(
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => array(
        //'class' => array('button'),
      ),
      //'#weight' => 0,
      '#url' => Url::fromRoute('event_form.step_two'),
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      //'#button_type' => 'primary',
      //'#weight' => 10,
    );
      return $form;
    }
  
    /**
    * @param array $form
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    */
    function mymoduleAjaxExampleAddMoreRemove(array &$form, FormStateInterface $form_state) {
      // Get the triggering item
      $delta_remove = $form_state->getTriggeringElement()['#parents'][1];
      
      // Store our form state
      $field_deltas_array = $form_state->get('field_deltas');
      
      // Find the key of the item we need to remove
      $key_to_remove = array_search($delta_remove, $field_deltas_array);
      
      // Remove our triggered element
      unset($field_deltas_array[$key_to_remove]);
      
      // Rebuild the field deltas values
      $form_state->set('field_deltas', $field_deltas_array);
      
      // Rebuild the form
      $form_state->setRebuild();
      
      // Return any messages set
      drupal_get_messages();
    }
  
    /**
    * @param array $form
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    *
    * @return mixed
    */
    function mymoduleAjaxExampleAddMoreRemoveCallback(array &$form, FormStateInterface $form_state) {
      return $form['field_container'];
    }
  
    /**
    * @param array $form
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    */
    function mymoduleAjaxExampleAddMoreAddOne(array &$form, FormStateInterface $form_state) {
  
      // Store our form state
      $field_deltas_array = $form_state->get('field_deltas');
      
      // check to see if there is more than one item in our array
      if (count($field_deltas_array) > 0) {
        // Add a new element to our array and set it to our highest value plus one
        $field_deltas_array[] = max($field_deltas_array) + 1;
      }
      else {
        // Set the new array element to 0
        $field_deltas_array[] = 0;
      }
    
      // Rebuild the field deltas values
      $form_state->set('field_deltas', $field_deltas_array);
    
      // Rebuild the form
      $form_state->setRebuild();
    
      // Return any messages set
      drupal_get_messages();
    }
  
    /**
    * @param array $form
    * @param \Drupal\Core\Form\FormStateInterface $form_state
    *
    * @return mixed
    */
    function mymoduleAjaxExampleAddMoreAddOneCallback(array &$form, FormStateInterface $form_state) {
      return $form['field_container'];
    }
  
    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $form_state->setRedirect('event_form.step_four');
      // Submit Form    
    } 

}