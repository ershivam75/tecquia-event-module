<?php

/**
 * @file
 * Contains \Drupal\event_form\Form\EventStepOneForm.
*/

namespace Drupal\event_form\Form;

//use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class EventStepOneForm extends MultistepEventForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'event_step_one_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //$form = parent::buildForm($form, $form_state);

    $form['event_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("What's the name of your event?"),
      '#required' => TRUE,
      '#attributes' => array(
        'placeholder' => t('Name of Your Event'),
      ),
    );

    $form['event_description'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('How would you describe your event?'),
      '#attributes' => array(
        'placeholder' => t('Description of Your Event'),
      ),
      '#resizable' => FALSE,
    );

    $form['multiple_events'] = array(
      '#type' => 'checkbox',
      '#title' => t('Schedule multiple events'),
      '#ajax' => [
        'callback' => '::multipleEventCallback',
        'wrapper' => 'multievent-wrapper-container',
        'effect' => 'fade',
      ],
    );

    $form['multievent_wrapper_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'multievent-wrapper-container'],
    ];
    $form['multievent_wrapper_container']['textfields'] = [
      '#type' => 'container',
    ];

    if ($form_state->getValue('multiple_events', NULL) === 1) {

      $values = array(
        '' => t('How Often Does This Event Occur?'),
        'Daily' => t('Daily'),
        'Weekly' => t('Weekly'),
        'Monthly' => t('Monthly'),
        'custom' => t('custom'),
      );
      $form['multievent_wrapper_container']['event_occur'] = array(
        '#type' => 'select',
        '#options' => $values,
        '#ajax' => [
          'wrapper' => 'multiple-event-occur',
          'callback' => '::multipleEventOccurCallback',
        ],
      );

      $form['multievent_wrapper_container']['event_occur_date_time'] = array(
        '#type' => 'container',
        '#attributes' => ['id' => 'multiple-event-occur'],
      );

      $event_occur = $form_state->getValue('event_occur');
      if (!empty($event_occur) && $event_occur !== '') {

        $form['multievent_wrapper_container']['event_occur_date_time']['select_by_week_day'] = array(
          '#type' => 'checkbox',
          '#title' => t('Select by day of the week')
        );

        $form['multievent_wrapper_container']['event_occur_date_time']['select_by_month_day'] = array(
          '#type' => 'checkbox',
          '#title' => t('Select by day of the month')
        );
        $values1 = array(
          '' => t('Repeating Every'),
          'Daily' => t('First'),
          'Weekly' => t('Secound'),
          'Monthly' => t('Third'),
          'Fourth' => t('Fourth'),
        );
        $form['multievent_wrapper_container']['event_occur_date_time']['repeating_every'] = array(
          '#type' => 'select',
          '#options' => $values1,
        );

        $values2 = array(
          '' => t('Day(S) of The Week'),
          'Sunday' => t('Sunday'),
          'Monday' => t('Monday'),
          'Tuesday' => t('Tuesday'),
          'Wednesday' => t('Wednesday'),
          'Thuresday' => t('Thuresday'),
          'Friday' => t('Friday'),
          'Saturday' => t('Saturday'),
        );

        $form['multievent_wrapper_container']['event_occur_date_time']['week_days'] = array(
          '#type' => 'select',
          '#options' => $values2,
        );

        $form['multievent_wrapper_container']['event_occur_date_time']['from_time'] = array(
          '#type' => 'datetime',
          '#title' => $this->t('From'),
          '#size' => 20,
          '#date_date_element' => 'none', // hide date element
          '#date_time_element' => 'time', // you can use text element here as well
          '#date_time_format' => 'H:i:s a',
          '#default_value' => '00:00:00',
        );
        $form['multievent_wrapper_container']['event_occur_date_time']['to_time'] = array(
          '#type' => 'datetime',
          '#title' => $this->t('To'),
          '#size' => 20,
          '#date_date_element' => 'none', // hide date element
          '#date_time_element' => 'time', // you can use text element here as well
          '#date_time_format' => 'H:i:s a',
          '#default_value' => '00:00:00',
        );

        $values3 = array(
            '' => t('Of The'),
            'Same day' => t('Same day'),
            'Next day' => t('Next day'),
            '2nd day' => t('2nd day'),
            '3rd day' => t('3rd day'),
            '4th day' => t('4th day'),
            '5th day' => t('5th day'),
            '6th day' => t('6th day'),
          );
        $form['multievent_wrapper_container']['event_occur_date_time']['of_the'] = array(
          '#title' => t('Of The'),
          '#type' => 'select',
          '#options' => $values3,
        );

        $form['multievent_wrapper_container']['event_occur_date_time']['occurs_form'] = array(
          '#title' => $this->t('Occurs From'),
          '#type' => 'date',
          '#attributes' => array(
            'type'=> 'date', 
            'min'=> '-25 years', 
            'max' => '+5 years',
            'placeholder' => t('dd/mm/yyyy'), 
          ),
          '#date_date_format' => 'd/m/Y',
        );

        $form['multievent_wrapper_container']['event_occur_date_time']['occurs_until'] = array(
          '#title' => $this->t('Occurs Until'),
          '#type' => 'date',
          '#attributes' => array(
            'type'=> 'date', 
            'min'=> '-25 years', 
            'max' => '+5 years',
            'placeholder' => t('dd/mm/yyyy'), 
          ),
          '#date_date_format' => 'd/m/Y',
        );       
      }
    }
    else{
      $form['multievent_wrapper_container']['event_date_time'] = array(
        '#type' => 'container',
      );

      $form['multievent_wrapper_container']['event_date_time']['start_date'] = array(
        '#title' => $this->t('Start Date'),
        '#type' => 'date',
        '#attributes' => array(
          'type'=> 'date', 
          'min'=> '-25 years', 
          'max' => '+5 years',
          'placeholder' => t('dd/mm/yyyy'), 
        ),
        '#date_date_format' => 'd/m/Y',
        '#required' => TRUE,
      );

      $form['multievent_wrapper_container']['event_date_time']['end_date'] = array(
        '#title' => $this->t('End Date'),
        '#type' => 'date',
        '#attributes' => array(
          'type'=> 'date', 
          'min'=> '-25 years', 
          'max' => '+5 years',
          'placeholder' => t('dd/mm/yyyy'), 
        ),
        '#date_date_format' => 'd/m/Y',
        '#required' => TRUE,
      );

      $form['multievent_wrapper_container']['event_date_time']['start_time'] = array(
        '#type' => 'datetime',
        '#title' => $this->t('Start Time'),
        '#required' => TRUE,
        '#date_date_element' => 'none', // hide date element
        '#date_time_element' => 'time', // you can use text element here as well
        '#date_time_format' => 'H:i:s a',
        '#default_value' => '00:00:00',
      );
      $form['multievent_wrapper_container']['event_date_time']['end_time'] = array(
        '#type' => 'datetime',
        '#required' => TRUE,
        '#title' => $this->t('End Time'),
        '#date_date_element' => 'none', // hide date element
        '#date_time_element' => 'time', // you can use text element here as well
        '#date_time_format' => 'H:i:s a',
        '#default_value' => '00:00:00',
      );
    }

    $form['registration_deadline'] = [
      '#type' => 'container',
    ];

    $form['registration_deadline']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('When is your registartion deadline?'),
    ];

    $form['registration_deadline']['date'] = array(
      '#title' => t('Date'),
      '#type' => 'date',
      '#required' => TRUE,
      '#attributes' => array(
        'type'=> 'date', 
        'min'=> '-25 years', 
        'max' => '+5 years',
        'placeholder' => t('dd/mm/yyyy'), 
      ),
      '#date_date_format' => 'd/m/Y',
    );

    $form['registration_deadline']['time'] = array(
      '#type' => 'datetime',
      '#title' => t('Time'),
      '#size' => 20,
      '#required' => TRUE,
      '#date_date_element' => 'none', // hide date element
      '#date_time_element' => 'time', // you can use text element here as well
      '#date_time_format' => 'H:i:s a',
      '#default_value' => '00:00:00',
    );

    $form['registration_goal'] = array(
      '#type' => 'textfield',
      '#title' => t("What's your registration goal?"),
      '#required' => TRUE,
      '#attributes' => array(
        'placeholder' => t('Registration Goal'),
      ),
    );

    $form['event_image_video'] = [
      '#type' => 'managed_file',
      '#title' => t('Event Image & Video'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg mp4 ogv webm mov'),
      ),
      '#upload_location' => 'public://event-image-video',
    ];


    //$form['actions']['submit']['#value'] = $this->t('Next');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      //'#button_type' => 'primary',
      //'#weight' => 10,
    );

    return $form;
  }

  public function multipleEventCallback($form, FormStateInterface $form_state) {
    return $form['multievent_wrapper_container'];
  }

  public function multipleEventOccurCallback(array $form, FormStateInterface $form_state) {
    return $form['multievent_wrapper_container']['event_occur_date_time'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$multiple_events = $form_state->getValue('multiple_events');
    //kint($multiple_events); exit;
    // $fid = $form_state->getValue('event_image_video');
    // if (!empty($fid[0])) {
    //   $file = File::load($fid[0]);
    //   $url = $file->getFileUri();
    //   $file_path = file_url_transform_relative(file_create_url($url));;
    //   $file->setPermanent();
    //   $file->save(); 
    // }
    // $this->store->set('event_name', $form_state->getValue('event_name'));
    // $this->store->set('event_description', $form_state->getValue('event_description'));
    // $this->store->set('start_date', $form_state->getValue('start_date'));
    // $this->store->set('end_date', $form_state->getValue('end_date'));
    // $this->store->set('start_time', $form_state->getValue('start_time'));
    // $this->store->set('end_time', $form_state->getValue('end_time'));
    // $this->store->set('event_image_video', $file_path);
    // $this->store->set('event_image_video_id', $fid[0]);
    $form_state->setRedirect('event_form.step_two');
  }

}

