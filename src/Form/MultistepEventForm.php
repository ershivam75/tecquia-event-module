<?php

/**
 * @file
 * Contains \Drupal\event_form\Form\MultistepEventForm.
*/

namespace Drupal\event_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use \Drupal\node\Entity\Node;

abstract class MultistepEventForm extends FormBase {

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * Constructs a \Drupal\event_form\Form\MultistepEventForm.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   * @param \Drupal\Core\Session\AccountInterface $current_user
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;

    $this->store = $this->tempStoreFactory->get('event_form');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Start a manual session for anonymous users.
    if ($this->currentUser->isAnonymous() && !isset($_SESSION['event_form_holds_session'])) {
      $_SESSION['event_form_holds_session'] = true;
      $this->sessionManager->start();
    }

    $form = array();
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight' => 10,
    );

    return $form;
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function saveData() {
    // $user = \Drupal::currentUser();
    // $uid = $user->id();
    // $fid = $this->store->get('event_image_video_id'); // The fid of the image you are going to use.

    // $node = Node::create([
    //   'type' => 'event',
    //   'title' => $this->store->get('event_name'),
    //   'field_event_description' => $this->store->get('event_description'),
    //   'field_start_date' => $this->store->get('start_date'),
    //   'field_end_date' => $this->store->get('end_date'),
    //   'field_start_time' => $this->store->get('start_time'),
    //   'field_end_time' => $this->store->get('end_time'),
    //   'field_event_image_video' => ['target_id' => $fid],
    //   'field_event_venue' => $this->store->get('location'),
    // ]);

    // $node_result = $node->save();
    // if($node_result == TRUE){
    //   drupal_set_message('Node created Sucessfully.', 'status');
    // }
    drupal_set_message($this->t('The form has been saved.'));
  }
}