<?php

/**
 * @file
 * Contains \Drupal\event_form\Form\EventStepOneForm.
*/

namespace Drupal\event_form\Form;

//use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class EventStepOneForm extends MultistepEventForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'event_step_one_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#theme'] = 'event_step_one_form';
    // Disable caching for the form
    $form['#cache'] = ['max-age' => 0];
    
    $form['#tree'] = TRUE;

    // $form['#prefix'] = '<div class="create-events-wrap"><div class="cew-inner clearfix"><div class="create-event-right"><h1><span>Event</span> Information</h1>';
    // $form['#suffix'] = '</div></div></div>';

    $form['event_name'] = array(
      '#type' => 'textfield',
      '#title' => "<span>Waht's the</span> name of your event?",
      '#required' => TRUE,
      '#attributes' => array(
        'placeholder' => t('Name of Your Event'),
      ),
      '#prefix' => '<div class="single-field">',
      '#suffix' => '</div>',
    );

    $form['event_description'] = array(
      '#type' => 'textarea',
      '#title' => '<span>How would you</span> describe your event?',
      '#attributes' => array(
        'placeholder' => t('Description of Your Event'),
      ),
      '#resizable' => FALSE,
      '#rows' => 5,
      '#prefix' => '<div class="single-field">',
      '#suffix' => '</div>',
    );


    //if ($form_state->getValue('multiple_events', NULL) !== 1) {
      $form['event_date'] = array(
        '#type' => 'container',
        '#prefix' => '<div class="double-fileds"><div class="df-inner">',
        '#suffix' => '</div></div>',
        '#states' => array(
          'visible' => array(
            ':input[name=multiple_events]' => array("checked" => FALSE),
          ),
        ),
      );

      $form['event_date']['start_date'] = array(
        '#title' => '<span>Start</span> Date',
        '#type' => 'textfield',
        '#prefix' => '<div class="single-field">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'placeholder' => t('dd/mm/yyyy'),
          'id' => array('from'),
        ),
        '#states' => array(
          'required' => array(
            ':input[name="multiple_events"]' => ['checked' => FALSE],
          ),
        ),
      );

      $form['event_date']['end_date'] = array(
        '#title' => '<span>End</span> Date',
        '#type' => 'textfield',
        '#prefix' => '<div class="single-field">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'placeholder' => t('dd/mm/yyyy'),
          'id' => array('to'),
        ),
      );

      $form['event_time'] = array(
        '#type' => 'container',
        '#prefix' => '<div class="double-fileds"><div class="df-inner">',
        '#suffix' => '</div></div>',
        '#states' => array(
          'visible' => array(
            ':input[name=multiple_events]' => array("checked" => FALSE),
          ),
        ),
      );

      $form['event_time']['start_time'] = array(
        '#type' => 'textfield',
        '#title' => '<span>Start</span> Time',
        '#prefix' => '<div class="single-field">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'placeholder' => t('00:00 AM'),
          'id' => array('stepExample1'),
          'class' => array('time'),
        ),
      );
      $form['event_time']['end_time'] = array(
        '#type' => 'textfield',
        '#title' => '<span>End</span> Time',
        '#prefix' => '<div class="single-field">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'placeholder' => t('00:00 PM'),
          'id' => array('stepExample2'),
          'class' => array('time'),
        ),
      );
    //}


    $form['multiple_events'] = array(
      '#type' => 'checkbox',
      '#title' => '<p><span>Schedule</span> multiple events </p>',
      '#ajax' => [
        'callback' => '::multipleEventCallback',
        'wrapper' => 'multievent-wrapper-container',
        'effect' => 'fade',
      ],
      '#prefix' => '<div class="custom-check"><div class="cc-inner">',
      '#suffix' => '</div></div>',
    );

    $form['multievent_wrapper_container'] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#prefix' => '<div id="multievent-wrapper-container">',
      '#suffix' => '</div>',
      // '#states' => array(
      //   'visible' => array(
      //     ':input[name=multiple_events]' => array("checked" => TRUE),
      //   ),
      // ),
    );
    
    if ($form_state->getValue('multiple_events', NULL) === 1) {
      if ($form_state->get('field_deltas') == '') {
        $form_state->set('field_deltas', range(0, 0));
      }
      
      $field_count = $form_state->get('field_deltas');
    
      foreach ($field_count as $delta) {
        $form['multievent_wrapper_container'][$delta] = array(
          '#type' => 'container',
          '#attributes' => array(
            //'class' => array('container-inline'),
          ),
          '#tree' => TRUE,
        );

        $values = array(
          '' => t('How Often Does This Event Occur?'),
          'Daily' => t('Daily'),
          'Weekly' => t('Weekly'),
          'Monthly' => t('Monthly'),
          'custom' => t('custom'),
        );

        $form['multievent_wrapper_container'][$delta]['event_occur'] = [
          '#type' => 'select',
          //'#title' => $this->t('Select element'),
          '#options' => $values,
          '#attributes' => array(
            'id' => array('event-occur-'.$delta.'-select'),
            'class' => array('selectpicker'),
          ),
          '#prefix' => '<div class="single-field when-occur">',
          '#suffix' => '</div>',
        ];
        
        $form['multievent_wrapper_container'][$delta]['event_occur_date_time'] = array(
          '#type' => 'container',
          '#attributes' => array(
            //'class' => array('container-inline'),
          ),
          '#tree' => TRUE,
          '#states' => array(
            'invisible' => array(
              '#event-occur-'.$delta.'-select' => array("value" => ''),
            ),
          ),
          '#prefix' => '<div class="double-fileds mulitple-events">',
          '#suffix' => '</div>',
        );

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_multi_date'] = [
          '#type' => 'container',
          '#prefix' => '<div class="double-head">',
          '#suffix' => '</div>',
        ];

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_multi_date']['select_by_week_day'] = array(
          '#type' => 'checkbox',
          '#title' => '<p>Select by day of the week</p>',
          '#prefix' => '<div class="single-head"><div class="custom-check"><div class="cc-inner">',
          '#suffix' => '</div></div></div>',
        );

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_multi_date']['select_by_month_day'] = array(
          '#type' => 'checkbox',
          '#title' => '<p>Select by day of the month</p>',
          '#prefix' => '<div class="single-head"><div class="custom-check"><div class="cc-inner">',
          '#suffix' => '</div></div></div>',
        );
        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_multi_select'] = [
          '#type' => 'container',
          '#prefix' => '<div class="df-inner">',
          '#suffix' => '</div>',
        ];
        $values1 = array(
          '' => t('Repeating Every'),
          'Daily' => t('First'),
          'Weekly' => t('Secound'),
          'Monthly' => t('Third'),
          'Fourth' => t('Fourth'),
        );
        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_multi_select']['repeating_every'] = array(
          '#type' => 'select',
          '#options' => $values1,
          '#attributes' => array(
            'class' => array('selectpicker'),
          ),
          '#prefix' => '<div class="single-field">',
          '#suffix' => '</div>',
        );

        $values2 = array(
          //'' => t('Day(S) of The Week'),
          'Sunday' => t('Sunday'),
          'Monday' => t('Monday'),
          'Tuesday' => t('Tuesday'),
          'Wednesday' => t('Wednesday'),
          'Thuresday' => t('Thuresday'),
          'Friday' => t('Friday'),
          'Saturday' => t('Saturday'),
        );

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_multi_select']['week_days'] = array(
          '#type' => 'select',
          '#options' => $values2,
          '#attributes' => array(
            'class' => array('selectpicker'),
          ),
          '#multiple' => TRUE,
          '#prefix' => '<div class="single-field">',
          '#suffix' => '</div>',
        );

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_from_to'] = [
          '#type' => 'container',
          '#prefix' => '<div class="df-inner">',
          '#suffix' => '</div>',
        ];

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_from_to']['from_time'] = array(
          '#type' => 'textfield',
          '#title' => '<p>From</p>',
          '#prefix' => '<div class="single-field">',
          '#suffix' => '</div>',
          '#attributes' => array(
            'placeholder' => t('00:00 AM'),
            'id' => array('stepExample3'),
            'class' => array('time'),
          ),
        );
        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_from_to']['to_time'] = array(
          '#type' => 'textfield',
          '#title' => '<p>To</p>',
          '#prefix' => '<div class="single-field">',
          '#suffix' => '</div>',
          '#attributes' => array(
            'placeholder' => t('00:00 PM'),
            'id' => array('stepExample4'),
            'class' => array('time'),
          ),
        );

        $values3 = array(
            '' => t('Of The'),
            'Same day' => t('Same day'),
            'Next day' => t('Next day'),
            '2nd day' => t('2nd day'),
            '3rd day' => t('3rd day'),
            '4th day' => t('4th day'),
            '5th day' => t('5th day'),
            '6th day' => t('6th day'),
          );
        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['of_the'] = array(
          //'#title' => t('Of The'),
          '#type' => 'select',
          '#options' => $values3,
          '#attributes' => array(
            'class' => array('selectpicker'),
          ),
          '#prefix' => '<div class="single-field when-occur">',
          '#suffix' => '</div>',
        );

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_occurs'] = [
          '#type' => 'container',
          '#prefix' => '<div class="df-inner">',
          '#suffix' => '</div>',
        ];

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_occurs']['occurs_form'] = array(
          '#title' => '<span>Occurs</span> From',
          '#type' => 'textfield',
          '#prefix' => '<div class="single-field">',
          '#suffix' => '</div>',
          '#attributes' => array(
            'placeholder' => t('dd/mm/yyyy'),
            'id' => array('occurs_from'),
          ),
        );

        $form['multievent_wrapper_container'][$delta]['event_occur_date_time']['event_occurs']['occurs_until'] = array(
          '#title' => '<span>Occurs</span> Until',
          '#type' => 'textfield',
          '#prefix' => '<div class="single-field">',
          '#suffix' => '</div>',
          '#attributes' => array(
            'placeholder' => t('dd/mm/yyyy'),
            'id' => array('occurs_until'),
          ),
        );  

        $form['multievent_wrapper_container'][$delta]['remove_name'] = array(
          '#type' => 'submit',
          '#value' => t('Remove Date'),
          '#submit' => array('::multipleEventAddMoreRemove'),
          '#ajax' => array(
            'callback' => '::multipleEventAddMoreRemoveCallback',
            'wrapper' => 'multievent-wrapper-container',
          ),
          //'#weight' => -50,
          '#attributes' => array(
            'class' => array('button-small'),
          ),
          '#name' => 'remove_name_' . $delta,
        );
      }
    
      $form['multievent_wrapper_container']['add_name'] = array(
        '#type' => 'submit',
        '#value' => t('Add Date'),
        '#submit' => array('::multipleEventAddMoreAddOne'),
        '#ajax' => array(
          'callback' => '::multipleEventAddMoreAddOneCallback',
          'wrapper' => 'multievent-wrapper-container',
        ),
        '#prefix' => '<div class="bottom-buttons"><div class="single-button filled-btn">',
        '#suffix' => '</div></div>',
        //'#weight' => 100,
      );
    }

  
    $form['registration_deadline'] = [
      '#type' => 'container',
      '#prefix' => '<div class="double-fileds">',
      '#suffix' => '</div>',
    ];

    $form['registration_deadline']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '<span>When is your</span> registration deadlines?',
    ];
    $form['registration_deadline']['reg_field_set'] = [
      '#type' => 'container',
      '#prefix' => '<div class="df-inner">',
      '#suffix' => '</div>',
    ];

    $form['registration_deadline']['reg_field_set']['date'] = array(
      '#title' => '<span>Date</span>',
      '#type' => 'textfield',
      '#prefix' => '<div class="single-field">',
      '#suffix' => '</div>',
      '#attributes' => array(
        'id' => array('datepicker-registration'),
      ),
    );

    $form['registration_deadline']['reg_field_set']['time'] = array(
      '#type' => 'textfield',
      '#title' => '<span>Time</span>',
      '#prefix' => '<div class="single-field">',
      '#suffix' => '</div>',
      '#attributes' => array(
        'id' => array('scrollDefaultExample'),
        'class' => array('time'),
      ),
    );

    $form['registration_goal'] = array(
      '#type' => 'textfield',
      '#title' => '<span>What is your</span> registration goal?',
      //'#required' => TRUE,
      '#attributes' => array(
        'placeholder' => t('Registration Goal'),
      ),
      '#prefix' => '<div class="single-field">',
      '#suffix' => '</div>',
    );

    $form['event_image_wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div class="single-field upload-img">',
      '#suffix' => '</div>',
    ];

    $form['event_image_wrapper']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => '<span>Event</span> Image & Video',
    ];

    $form['event_image_wrapper']['event_image_video'] = [
      '#type' => 'file',
      '#title' => '<span><i class="fas fa-camera"></i><span><i>Add</i> Event Image</span></span>',
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg mp4 ogv webm mov'),
      ),
      '#upload_location' => 'public://event-image-video',
      '#prefix' => '<div class="file-upload"><div id="image-preview">',
      '#suffix' => '</div></div>',
      '#attributes' => array(
        'id' => array('image-upload'),
      ),
    ];

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#prefix' => '<div class="bottom-buttons"><div class="single-button filled-btn">',
      '#suffix' => '</div></div>',
    );

    return $form;
  }
  
  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  function multipleEventAddMoreRemove(array &$form, FormStateInterface $form_state) {
    // Get the triggering item
    $delta_remove = $form_state->getTriggeringElement()['#parents'][1];
    
    // Store our form state
    $field_deltas_array = $form_state->get('field_deltas');
    
    // Find the key of the item we need to remove
    $key_to_remove = array_search($delta_remove, $field_deltas_array);
    
    // Remove our triggered element
    unset($field_deltas_array[$key_to_remove]);
    
    // Rebuild the field deltas values
    $form_state->set('field_deltas', $field_deltas_array);
    
    // Rebuild the form
    $form_state->setRebuild();
    
    // Return any messages set
    drupal_get_messages();
  }
  
  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  function multipleEventAddMoreRemoveCallback(array &$form, FormStateInterface $form_state) {
    return $form['multievent_wrapper_container'];
  }
  
  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  function multipleEventAddMoreAddOne(array &$form, FormStateInterface $form_state) {
  
    // Store our form state
    $field_deltas_array = $form_state->get('field_deltas');
    
    // check to see if there is more than one item in our array
    if (count($field_deltas_array) > 0) {
      // Add a new element to our array and set it to our highest value plus one
      $field_deltas_array[] = max($field_deltas_array) + 1;
    }
    else {
      // Set the new array element to 0
      $field_deltas_array[] = 0;
    }
  
    // Rebuild the field deltas values
    $form_state->set('field_deltas', $field_deltas_array);
  
    // Rebuild the form
    $form_state->setRebuild();
  
    // Return any messages set
    drupal_get_messages();
  }
  
  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  function multipleEventAddMoreAddOneCallback(array &$form, FormStateInterface $form_state) {
    return $form['multievent_wrapper_container'];
  }

  public function multipleEventCallback($form, FormStateInterface $form_state) {
    return $form['multievent_wrapper_container'];
  }
  
  /**
   * {@inheritdoc}
   */
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$multiple_events = $form_state->getValue('multiple_events');
    //kint($multiple_events); exit;
    // $fid = $form_state->getValue('event_image_video');
    // if (!empty($fid[0])) {
    //   $file = File::load($fid[0]);
    //   $url = $file->getFileUri();
    //   $file_path = file_url_transform_relative(file_create_url($url));;
    //   $file->setPermanent();
    //   $file->save(); 
    // }
    // $this->store->set('event_name', $form_state->getValue('event_name'));
    // $this->store->set('event_description', $form_state->getValue('event_description'));
    // $this->store->set('start_date', $form_state->getValue('start_date'));
    // $this->store->set('end_date', $form_state->getValue('end_date'));
    // $this->store->set('start_time', $form_state->getValue('start_time'));
    // $this->store->set('end_time', $form_state->getValue('end_time'));
    // $this->store->set('event_image_video', $file_path);
    // $this->store->set('event_image_video_id', $fid[0]);
    $form_state->setRedirect('event_form.step_two');
  }

}

